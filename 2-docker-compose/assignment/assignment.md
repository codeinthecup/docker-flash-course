# Zadanie 2 - docker-compose
> Połaczenie małej PHP'owej aplikacji z webserwisem i bazą danych

## Wymagania
1. Aplikacja składa się z dwóch części - PHP i baza danych w tle (MariaDB lub MySQL)
2. Aplikacja łączy się z bazą danych przez PDO (należy je zainstalować przy tworzeniu Dockerfile)
3. Aplikacja bierze dane do połączenia z następujących zmiennych środowiskowych
    1. `ENV_DB_HOST` - nazwa hosta
    2. `ENV_DB_NAME` - nazwa bazy danych
    3. `ENV_DB_USER` - nazwa użytkownika
    4. `ENV_DB_PASS` - hasło
4. Każde z powyższych ustawień może również przyjąć formę ścieżki do pliku (np.: `ENV_DB_HOST_FILE`).
5. Sekrety bazy danych powinny być trzymane w plikach tekstowych na potrzeby środowiska developerskiego.
6. Aplikacja powinna być dostępna na porcie `8080` hosta i bindować się do portu `80` na swoim kontenerze. 

