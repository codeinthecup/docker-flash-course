<?php

function getFromFileOrEnv($prefix) {
    $file = getenv($prefix . '_FILE');

    if (!empty($file) && file_exists($file)) {
        return trim(file_get_contents($file));
    }

    return getenv($prefix);
}

function connect() {
    $host = getFromFileOrEnv('ENV_DB_HOST');
    $name = getFromFileOrEnv('ENV_DB_NAME');
    $user = getFromFileOrEnv('ENV_DB_USER');
    $pass = getFromFileOrEnv('ENV_DB_PASS');

    $query = <<<EOF
    CREATE TABLE IF NOT EXISTS entries (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `entry` varchar(255) NOT NULL DEFAULT '',
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
EOF;

    $pdo = new PDO("mysql:host=$host;dbname=$name", $user, $pass);
    $pdo->prepare($query)->execute();
    
    return $pdo;
}
