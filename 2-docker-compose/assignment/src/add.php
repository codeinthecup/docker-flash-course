<?php
require_once('utils.php');

if (!empty($_POST['post'])) {
    $pdo = connect();
    $stmt = $pdo->prepare('INSERT INTO entries (entry) VALUES (:value)');
    $stmt->bindParam(':value', $_POST['post']);
    $stmt->execute();

    echo '<h2> Post dodany! </h2>';
}

?>
<form method="post">
    <input type="text" name="post"/>
    <input type="submit" value="Dodaj"/>
</form>
