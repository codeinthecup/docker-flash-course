# Zadanie: Nasza mała php-owa apka w swarmie!
> Połaczenie małej PHP'owej aplikacji z webserwisem i bazą danych

## Wymagania
> Punkty 1-6 takie jak w docker-compose
1. Aplikacja składa się z dwóch części - PHP i baza danych w tle (MariaDB lub MySQL)
2. Aplikacja łączy się z bazą danych przez PDO (należy je zainstalować przy tworzeniu Dockerfile)
3. Aplikacja bierze dane do połączenia z następujących zmiennych środowiskowych
    1. `ENV_DB_HOST` - nazwa hosta
    2. `ENV_DB_NAME` - nazwa bazy danych
    3. `ENV_DB_USER` - nazwa użytkownika
    4. `ENV_DB_PASS` - hasło
4. Każde z powyższych ustawień może również przyjąć formę ścieżki do pliku (np.: `ENV_DB_HOST_FILE`).
5. Sekrety bazy danych powinny być trzymane w plikach tekstowych na potrzeby środowiska developerskiego.
6. Aplikacja powinna być dostępna na porcie `8080` hosta i bindować się do portu `80` na swoim kontenerze. 
7. Mamy 2 node'y - workera i managera
8. Baza danych NIE-znajduje się na swarm managerze
9. PHPowa apka jest na managerze i ma dwie repliki
10. PHPowa apka automatycznie uruchamia się ponownie w przypadku wyłączenia się w wyniku błędu.
11. Hasła są zarządzane przez swarm managera (NIE Z PLIKU!)

W swarmie nie budujemy swoich aplikacji. Gotowy obraz znajdziecie tutaj:
https://hub.docker.com/r/wunsz/phpexample/

Powodzenia!