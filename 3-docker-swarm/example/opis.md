# Zadanie: Wieloserwisowa, wielowęzłowa aplikacja do głosowania

> TO ZADANIE WYKONJEMY NA https://labs.play-with-docker.com/
> Do zalogowania się na play-with-docker potrzebne jest konto na dockerhub.

```
    ┌──────────────────────┐      ┌───────┐ 
    │ Voting App (Python)  │─────>│ Redis │─────────────┐
    └──────────────────────┘      └───────┘             🠧
                                                ┌─────────────────┐
                                                │  Worker (.NET)  │
                                                └─────────────────┘
    ┌──────────────────────┐      ┌───────────────┐     │
    │ Results App (NodeJS) │<─────│ DB PostgreSQL │<────┘
    └──────────────────────┘      └───────────────┘

```
- Powyższy diagram przedstawia jak będą działać serwisy
- Wszystkie obrazy są na Docker Hubie
- Wymagane są dwie sieci `backend` i `frontend` 
- Serwer bazy danych powinien używać nazwanego wolumenu (named volume) dla przechowywania danych

### Serwisy (nazwijcie je tak, jak w nagłówkach)
- `vote`
    - `dockersamples/examplevotingapp_vote:before`
    - Webowy frontend dla użytkowników do głosowania pies/kot
    - Idealnie jeśli będzie opublikowany na porcie 80
    - Na sieci frontendowej
    - 2+ repliki tego kontenera
    - 
- `redis`
    - `redis:3.2`
    - Baza klucz-wartość dla przychodzących głosów
    - Brak publicznych portów
    - Na sieci frontendowej
    - 1 replika
  
- `worker`
    - `dockersamples/examplevotingapp_worker`
    - Backendowy procesor czytający Redisa i przechowujący dane w Postgresie
    - Brak publicznych portów
    - W obu sieciach (backend i frontend)
    - 1 replika

- `db`
    - `postgres:9.4`
    - Jeden nazwany wolumen, wskazujacy na /var/lib/postgresql/data
    - Na sieci `backend`
    - 1 replika

- `result`
    - `dockersamples/examplevotingapp_result:before`
    - Aplikacja pokazująca wyniki
    - Działa na wysokim porcie (np. 5001 - kontener słucha na 80)
    - Na sieci Backend
    - 1 replika