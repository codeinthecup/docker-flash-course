# Zadanie 1 - Dockerfile
> Stworzenie bazowej aplikacji PHP z gotowego kodu

## Wymagania
1. Aplikacja działa na PHP `7.2.x` z serwerem Apache (użyjcie oficjalnego obrazu od PHP)
2. Apache słucha na porcie 80 ale na aplikację powinniśmy wchodzić przez `localhost:8080`
3. Aplikacja, pod adresem `localhost:8080/factorial.php?number=X` wylicza silnię z liczby X (kod jest gotowy, wymaga biblioteki bc-math)

> Punkt 3 może sprawiać mały problem, gdyż samo instalowanie rozszerzeń działa trochę inaczej. Znajdziecie wszystko w opisie oficjalnego obrazu na dockerhub.